import  React, { useState } from 'react';

const Search = ({ getQuery}) => {
    const [searchInput, setsearchInput] = useState('');
    const onChange = (q) =>{
        setsearchInput(q);
        getQuery(q);
    }
    // const [searchRes, setsearchRes] = useState([]);
    // const searchItems = (e)=>{
    //      setsearchInput(e.target.value);
    //    const newResults  = items.filter((val) => {
    //        if(e.target.value == val.name){
               
    //            return val
    //        }
    //        return val.name.toLowerCase().includes(searchInput.toLowerCase())
            
    //    })
    //    console.log(newResults)
    //     // setsearchRes(newResults)
    // }

    return(
        <section className="search">
            <form>
                <input type="search" placeholder="Search" className="form-control" value={searchInput} onChange={(e) => onChange(e.target.value) }/>
            </form>
        </section>

    )
}
export default Search;