import  React from 'react';
import CharcterItem from './CharcterItem';

const CharcterGrid = ({items,isloading}) => {
    return isloading ? (<h1>loading ...</h1>):(
    <section className="cards">
        {items.map(item =>{
            return(
                <CharcterItem key={item.char_id} item={item}/>
            )
        })}
    </section>
    )
}
export default CharcterGrid;