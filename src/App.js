import axios from 'axios';
import { useEffect, useState } from 'react';
import './App.css';
import Header from './Components/Ui/Header';
import CharcterGrid from './Components/charcters/CharcterGrid';
import Search from './Components/Ui/Search';
const  App = () => {
  const [items,setItems] = useState([]);
  const [isloading,setisloading] = useState(true);
  const [query,setQuery] = useState('');

  useEffect(()=>{
    const fetchItems = async ()=>{
      const result = await axios.get(`https://www.breakingbadapi.com/api/characters?name=${query}`);
      // console.log(result);
      setItems(result.data);
      setisloading(false);
    }
    fetchItems();
  },[query])
  const getQuery = (q) =>{
    setQuery(q)
  }
  return (
    <div className="container">
      <Header/>
      <Search items={items} getQuery={getQuery}/>
      <CharcterGrid items={items} isloading={isloading}/>
    </div>
  );
}

export default App;
